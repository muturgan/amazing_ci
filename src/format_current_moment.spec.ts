import { formatCurrentMoment } from './format_current_moment';


describe('formatCurrentMoment', () =>
{
   test('output length should be 10', () =>
   {
      const result = formatCurrentMoment();
      expect(typeof result).toBe('string');
      expect(result.length).toBe(10);
   });
});