
import http = require('http');
import { formatCurrentMoment } from './format_current_moment';

const PORT = 7777;

const contentType = {'Content-Type': 'text/plain'};

http.createServer((req, res) =>
{
   switch (req.url)
   {
      case '/':
         res.writeHead(200, contentType);
         res.end('Hello world!', 'utf-8');
         return;

      case '/moment':
         res.writeHead(200, contentType);
         res.end(formatCurrentMoment(), 'utf-8');
         return;

      case '/favicon.ico':
         res.writeHead(204);
         res.end();
         return;

      default:
         res.writeHead(404, contentType);
         res.end('Not found', 'utf-8');
   }

}).listen(PORT);

console.info(`Server running at http://127.0.0.1:${PORT}/`);