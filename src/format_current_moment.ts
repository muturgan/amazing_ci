import moment from 'moment';

export const formatCurrentMoment = (): string =>
{
   return moment().format('YYYY-MM-DD');
};