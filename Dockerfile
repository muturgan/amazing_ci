FROM node:14.15.0-alpine3.12

WORKDIR /app

COPY package*.json ./
RUN npm ci --only=production
COPY dist ./

EXPOSE 7777

CMD [ "node", "dist/server.js" ]